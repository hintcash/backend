INSERT INTO truck(license_plate)
VALUES ('99-AA-99');

INSERT INTO location(truck_id, lat, lng, date_creation)
VALUES
((SELECT id FROM truck WHERE license_plate = '99-AA-99'), 38.725181, -9.150658, current_date() - 1),
((SELECT id FROM truck WHERE license_plate = '99-AA-99'), 38.725733, -9.150121, current_date() - 2),
((SELECT id FROM truck WHERE license_plate = '99-AA-99'), 38.726151, -9.149714, current_date() - 3),
((SELECT id FROM truck WHERE license_plate = '99-AA-99'), 38.726536, -9.149435, current_date() - 4),
((SELECT id FROM truck WHERE license_plate = '99-AA-99'), 38.726536, -9.149435, current_date() - 5),
((SELECT id FROM truck WHERE license_plate = '99-AA-99'), 38.726686, -9.149279, current_date() - 6),
((SELECT id FROM truck WHERE license_plate = '99-AA-99'), 38.727094, -9.149027, current_date() - 7),
((SELECT id FROM truck WHERE license_plate = '99-AA-99'), 38.727601, -9.148810, current_date() - 8),
((SELECT id FROM truck WHERE license_plate = '99-AA-99'), 38.727925, -9.148558, current_date() - 9);