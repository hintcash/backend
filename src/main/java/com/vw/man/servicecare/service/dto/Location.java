package com.vw.man.servicecare.service.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class Location {
    private Double lat;
    private Double lng;
}
