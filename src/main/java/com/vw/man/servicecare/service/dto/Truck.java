package com.vw.man.servicecare.service.dto;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class Truck {
    private String licensePlate;
    private Location currentLocation;
    private Location firstLocation;
    private List<Location> path;
}
