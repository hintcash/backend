package com.vw.man.servicecare.service.impl;

import com.vw.man.servicecare.controller.dto.SaveTruckLocation;
import com.vw.man.servicecare.exception.TruckNotFoundException;
import com.vw.man.servicecare.helper.Converter;
import com.vw.man.servicecare.repository.TruckRepository;
import com.vw.man.servicecare.service.TruckService;
import com.vw.man.servicecare.service.dto.Truck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TruckServiceImpl implements TruckService {
    private final TruckRepository truckRepository;

    @Autowired
    public TruckServiceImpl(TruckRepository truckRepository) {
        this.truckRepository = truckRepository;
    }

    @Override
    public Truck findByLicensePlate(String licensePlate) {
        return Converter.truckEntityToTruck(truckRepository.findByLicensePlateIgnoreCase(licensePlate).orElseThrow(TruckNotFoundException::new));
    }

    @Override
    public Truck save(SaveTruckLocation saveTruckLocation) {
        return Converter.truckEntityToTruck(truckRepository.save(Converter.saveTruckLocationToTruckEntity(saveTruckLocation)));
    }

}
