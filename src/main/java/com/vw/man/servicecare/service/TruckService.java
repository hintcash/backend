package com.vw.man.servicecare.service;

import com.vw.man.servicecare.controller.dto.SaveTruckLocation;
import com.vw.man.servicecare.service.dto.Truck;

public interface TruckService {
    Truck findByLicensePlate(String licensePlate);

    Truck save(SaveTruckLocation truck);
}
