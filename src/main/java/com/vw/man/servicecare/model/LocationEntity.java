package com.vw.man.servicecare.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "location")
public class LocationEntity extends PersistentEntity {
    private Double lat;
    private Double lng;
    private LocalDateTime dateCreation;
}
