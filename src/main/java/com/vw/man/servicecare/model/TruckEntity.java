package com.vw.man.servicecare.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "truck")
public class TruckEntity extends PersistentEntity {
    private String licensePlate;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "truck_id")
    private List<LocationEntity> path;
}
