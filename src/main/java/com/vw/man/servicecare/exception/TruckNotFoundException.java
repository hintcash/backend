package com.vw.man.servicecare.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Truck not found")
public class TruckNotFoundException extends RuntimeException {
}
