package com.vw.man.servicecare.controller;

import com.vw.man.servicecare.controller.dto.SaveTruckLocation;
import com.vw.man.servicecare.service.TruckService;
import com.vw.man.servicecare.service.dto.Truck;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("truck")
@RestController
@Api(value = "Truck")
public class TruckController {

    private final TruckService truckService;

    @Autowired
    public TruckController(TruckService truckService) {
        this.truckService = truckService;
    }

    @ApiOperation(value = "Find truck paths by license plate")
    @GetMapping("{licensePlate}")
    public ResponseEntity<Truck> findByLicensePlate(@PathVariable String licensePlate) {
        return ResponseEntity.ok(truckService.findByLicensePlate(licensePlate));
    }

    @ApiOperation(value = "Save truck paths with license plate")
    @PostMapping
    public ResponseEntity<Truck> saveTruckLocation(@Validated @RequestBody SaveTruckLocation truck) {
        return ResponseEntity.status(HttpStatus.CREATED).body(truckService.save(truck));
    }
}
