package com.vw.man.servicecare.controller.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class SaveTruckLocation {
    @NotNull(message = "License plate is mandatory")
    private String licensePlate;
    @Valid
    @NotEmpty(message = "A path needs to be specified")
    private List<TruckLocation> path;
}
