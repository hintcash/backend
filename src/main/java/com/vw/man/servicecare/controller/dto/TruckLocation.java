package com.vw.man.servicecare.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class TruckLocation {
    @NotNull(message = "A lat needs to be specified")
    private Double lat;
    @NotNull(message = "A lng needs to be specified")
    private Double lng;
    @NotNull(message = "A read time needs to be specified")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateCreation;
}
