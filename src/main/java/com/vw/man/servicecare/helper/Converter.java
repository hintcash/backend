package com.vw.man.servicecare.helper;

import com.vw.man.servicecare.controller.dto.SaveTruckLocation;
import com.vw.man.servicecare.controller.dto.TruckLocation;
import com.vw.man.servicecare.model.LocationEntity;
import com.vw.man.servicecare.model.TruckEntity;
import com.vw.man.servicecare.service.dto.Location;
import com.vw.man.servicecare.service.dto.Truck;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Converter {

    public static Truck truckEntityToTruck(TruckEntity truckEntity) {
        List<LocationEntity> path = truckEntity.getPath();
        return Truck.builder()
                .licensePlate(truckEntity.getLicensePlate())
                .firstLocation(firstLocationAndRemoveFromPath(path))
                .currentLocation(currentLocationAndRemoveFromPath(path))
                .path(locationEntityPathToLocationPath(path))
                .build();
    }

    private static Location currentLocationAndRemoveFromPath(List<LocationEntity> path) {
        LocationEntity locationEntity = path.stream()
                .max(Comparator.comparing(LocationEntity::getDateCreation))
                .orElse(new LocationEntity());
        path.remove(locationEntity);
        return locationEntityToLocation(locationEntity);
    }

    private static Location firstLocationAndRemoveFromPath(List<LocationEntity> path) {
        LocationEntity locationEntity = path.stream()
                .min(Comparator.comparing(LocationEntity::getDateCreation))
                .orElse(new LocationEntity());
        path.remove(locationEntity);
        return locationEntityToLocation(locationEntity);
    }

    private static List<Location> locationEntityPathToLocationPath(List<LocationEntity> path) {
        return path.stream()
                .map(Converter::locationEntityToLocation)
                .collect(Collectors.toList());
    }

    private static Location locationEntityToLocation(LocationEntity locationEntity) {
        return Location.builder()
                .lat(locationEntity.getLat())
                .lng(locationEntity.getLng())
                .build();
    }

    public static TruckEntity saveTruckLocationToTruckEntity(SaveTruckLocation saveTruckLocation) {
        TruckEntity truckEntity = new TruckEntity();
        truckEntity.setLicensePlate(saveTruckLocation.getLicensePlate());
        truckEntity.setPath(truckLocationsToLocationEntityPath(saveTruckLocation.getPath()));
        return truckEntity;
    }

    private static List<LocationEntity> truckLocationsToLocationEntityPath(List<TruckLocation> truckLocations) {
        return truckLocations.stream().map(Converter::truckLocationToLocationEntity).collect(Collectors.toList());
    }

    private static LocationEntity truckLocationToLocationEntity(TruckLocation truckLocation) {
        LocationEntity locationEntity = new LocationEntity();
        locationEntity.setLng(truckLocation.getLng());
        locationEntity.setLat(truckLocation.getLat());
        locationEntity.setDateCreation(truckLocation.getDateCreation());
        return locationEntity;
    }
}
