package com.vw.man.servicecare.repository;

import com.vw.man.servicecare.model.TruckEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.Optional;

public interface TruckRepository extends JpaRepository<TruckEntity, BigInteger> {
    Optional<TruckEntity> findByLicensePlateIgnoreCase(String licensePlate);
}
