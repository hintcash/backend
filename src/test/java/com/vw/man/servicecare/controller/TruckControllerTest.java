package com.vw.man.servicecare.controller;

import com.vw.man.servicecare.exception.TruckNotFoundException;
import com.vw.man.servicecare.helper.BuildTrucker;
import com.vw.man.servicecare.service.TruckService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TruckController.class)
public class TruckControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TruckService truckService;

    @Test
    public void findByLicensePlate_returnTruckSuccessfully() throws Exception {
        when(truckService.findByLicensePlate(anyString())).thenReturn(BuildTrucker.buildTruck());
        this.mockMvc
                .perform(get("/truck/11-AA-11"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.currentLocation.lat").value(BuildTrucker.CURRENT_LOCATION_LAT))
                .andExpect(jsonPath("$.currentLocation.lng").value(BuildTrucker.CURRENT_LOCATION_LNG))
                .andExpect(jsonPath("$.firstLocation.lat").value(BuildTrucker.FIRST_LOCATION_LAT))
                .andExpect(jsonPath("$.firstLocation.lng").value(BuildTrucker.FIRST_LOCATION_LGN));
    }

    @Test
    public void findByLicensePlate_return404() throws Exception {
        Mockito.doThrow(TruckNotFoundException.class).when(truckService).findByLicensePlate("11-AA-11");
        this.mockMvc
                .perform(get("/truck/11-AA-11"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void saveTruckLocation_withNoParamAndReturn400() throws Exception {
        this.mockMvc
                .perform(post("/truck"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void saveTruckLocation_withLicensePlateWithoutPathAndReturn400() throws Exception {
        String content = "{\"licensePlate\": \"99-AA-99\"}]}";
        this.mockMvc
                .perform(post("/truck")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }


    @Test
    public void saveTruckLocation_return201() throws Exception {
        String content = "{\"licensePlate\": \"99-AA-99\", \"path\": [{\"lat\": 1, \"lng\": 1, \"dateCreation\": \"2010-01-01 00:00:00\"}]}";

        this.mockMvc
                .perform(post("/truck")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andDo(print())
                .andExpect(status().isCreated());
    }
}
