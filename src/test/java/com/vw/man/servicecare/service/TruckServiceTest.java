package com.vw.man.servicecare.service;

import com.vw.man.servicecare.controller.dto.SaveTruckLocation;
import com.vw.man.servicecare.helper.Converter;
import com.vw.man.servicecare.exception.TruckNotFoundException;
import com.vw.man.servicecare.helper.BuildTrucker;
import com.vw.man.servicecare.model.TruckEntity;
import com.vw.man.servicecare.repository.TruckRepository;
import com.vw.man.servicecare.service.dto.Truck;
import com.vw.man.servicecare.service.impl.TruckServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class TruckServiceTest {
    @InjectMocks
    private TruckServiceImpl truckService;
    @Mock
    private TruckRepository truckRepository;

    @Test
    public void findByLicensePlate_returnTruckOptional() {
        when(truckRepository.findByLicensePlateIgnoreCase(anyString())).thenReturn(Optional.of(BuildTrucker.buildTruckEntity()));
        Truck truck = truckService.findByLicensePlate("123");
        assertEquals(Double.valueOf(BuildTrucker.FIRST_LOCATION_LAT), truck.getFirstLocation().getLat());
        assertEquals(Double.valueOf(BuildTrucker.FIRST_LOCATION_LGN), truck.getFirstLocation().getLng());
        assertEquals(Double.valueOf(BuildTrucker.CURRENT_LOCATION_LAT), truck.getCurrentLocation().getLat());
        assertEquals(Double.valueOf(BuildTrucker.CURRENT_LOCATION_LNG), truck.getCurrentLocation().getLng());
    }

    @Test(expected = TruckNotFoundException.class)
    public void findByLicensePlate_returnEmptyOptional() {
        when(truckRepository.findByLicensePlateIgnoreCase(anyString())).thenReturn(Optional.empty());
        truckService.findByLicensePlate("123");
    }

    @Test
    public void save_success() {
        SaveTruckLocation saveTruckLocation = BuildTrucker.buildSaveTruckLocation();
        when(truckRepository.save(any(TruckEntity.class))).thenReturn(Converter.saveTruckLocationToTruckEntity(saveTruckLocation));
        Truck truck = truckService.save(saveTruckLocation);

        verify(truckRepository, times(1)).save(any(TruckEntity.class));

        assertEquals(BuildTrucker.LICENSE_PLATE, truck.getLicensePlate());
        assertEquals(Double.valueOf(BuildTrucker.FIRST_LOCATION_LAT), truck.getFirstLocation().getLat());
        assertEquals(Double.valueOf(BuildTrucker.FIRST_LOCATION_LGN), truck.getFirstLocation().getLng());
        assertEquals(Double.valueOf(BuildTrucker.CURRENT_LOCATION_LAT), truck.getCurrentLocation().getLat());
        assertEquals(Double.valueOf(BuildTrucker.CURRENT_LOCATION_LNG), truck.getCurrentLocation().getLng());
    }
}
