package com.vw.man.servicecare.helper;

import com.vw.man.servicecare.controller.dto.SaveTruckLocation;
import com.vw.man.servicecare.controller.dto.TruckLocation;
import com.vw.man.servicecare.model.LocationEntity;
import com.vw.man.servicecare.model.TruckEntity;
import com.vw.man.servicecare.service.dto.Location;
import com.vw.man.servicecare.service.dto.Truck;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class BuildTrucker {
    public static final double FIRST_LOCATION_LAT = 1d;
    public static final double FIRST_LOCATION_LGN = 1d;
    public static final double CURRENT_LOCATION_LAT = 2d;
    public static final double CURRENT_LOCATION_LNG = 4d;
    public static final String LICENSE_PLATE = "00-AA-00";

    public static TruckEntity buildTruckEntity() {
        TruckEntity truckEntity = new TruckEntity();
        truckEntity.setPath(buildLocationEntities());
        return truckEntity;
    }

    static List<LocationEntity> buildLocationEntities() {
        List<LocationEntity> locationEntities = new ArrayList<>();
        locationEntities.add(buildLocationEntity(1d, 3d, 2));
        locationEntities.add(buildLocationEntity(FIRST_LOCATION_LAT, FIRST_LOCATION_LGN, 4));
        locationEntities.add(buildLocationEntity(CURRENT_LOCATION_LAT, CURRENT_LOCATION_LNG, 1));
        locationEntities.add(buildLocationEntity(1d, 2d, 3));
        return locationEntities;
    }

    static LocationEntity buildLocationEntity(Double lat, Double lng, int amountToSubtract) {
        LocationEntity locationEntity = new LocationEntity();
        locationEntity.setDateCreation(LocalDateTime.now().minus(amountToSubtract, ChronoUnit.MINUTES));
        locationEntity.setLat(lat);
        locationEntity.setLng(lng);
        return locationEntity;
    }


    public static Truck buildTruck() {
        return Truck.builder()
                .currentLocation(buildLocation(CURRENT_LOCATION_LAT, CURRENT_LOCATION_LNG))
                .firstLocation(buildLocation(FIRST_LOCATION_LAT, FIRST_LOCATION_LGN))
                .path(buildLocations())
                .build();
    }

    static Location buildLocation(Double lat, Double lng) {
        return Location.builder().lat(lat).lng(lng).build();
    }

    static List<Location> buildLocations() {
        List<Location> locations = new ArrayList<>();
        locations.add(buildLocation(1d, 3d));
        locations.add(buildLocation(1d, 2d));
        return locations;
    }

    public static SaveTruckLocation buildSaveTruckLocation() {
        SaveTruckLocation saveTruckLocation = new SaveTruckLocation();
        saveTruckLocation.setLicensePlate(LICENSE_PLATE);
        saveTruckLocation.setPath(buildTruckLocations());
        return saveTruckLocation;
    }

    static List<TruckLocation> buildTruckLocations() {
        List<TruckLocation> truckLocations = new ArrayList<>();
        truckLocations.add(buildTruckLocation(FIRST_LOCATION_LAT, FIRST_LOCATION_LGN, 4));
        truckLocations.add(buildTruckLocation(1, 2, 3));
        truckLocations.add(buildTruckLocation(2, 3, 2));
        truckLocations.add(buildTruckLocation(CURRENT_LOCATION_LAT, CURRENT_LOCATION_LNG, 1));
        return truckLocations;
    }

    static TruckLocation buildTruckLocation(double lat, double lgn, int amountToSubtract) {
        TruckLocation truckLocation = new TruckLocation();
        truckLocation.setLat(lat);
        truckLocation.setLng(lgn);
        truckLocation.setDateCreation(LocalDateTime.now().minus(amountToSubtract, ChronoUnit.MINUTES));
        return truckLocation;
    }
}
