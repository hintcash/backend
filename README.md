API documentation: https://volks-test-backend.herokuapp.com/swagger-ui.html.

The database being used in the project is the H2, there's a data.sql file in the resources folder. 
In the application's bootstrap, a sample is inserted with a license plate (99-aa-99).
To a better instruction on how to insert new records, please look into the API documentation.

heroku endpoint: https://volks-test-backend.herokuapp.com/

Obs: For the heroku environment, it can take a while to startup the application.
 The free version puts the application to sleep after a while without any request.