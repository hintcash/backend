FROM maven:3.6.3-jdk-11-slim

WORKDIR /app
COPY ./pom.xml /app
COPY ./src /app/src

RUN mvn clean package -DskipTests

ENTRYPOINT [ "sh", "-c", "java -jar target/*.jar"]